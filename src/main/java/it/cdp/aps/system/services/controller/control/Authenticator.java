/*
 * Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package it.cdp.aps.system.services.controller.control;

import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.services.controller.ControllerManager;
import com.agiletec.aps.system.services.controller.control.AbstractControlService;
import com.agiletec.aps.system.services.user.IAuthenticationProviderManager;
import com.agiletec.aps.system.services.user.IUserManager;
import com.agiletec.aps.system.services.user.UserDetails;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAuthzResponse;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;

/**
 * Sottoservizio di controllo esecutore dell'autenticazione.
 *
 * @author M.Diana - E.Santoboni
 */
public class Authenticator extends AbstractControlService {

	private static final Logger _logger = LoggerFactory.getLogger(Authenticator.class);

	private String _adfsLocation;
	private String _adfsClientId;
	private String _adfsRedirectUrl;
	private String _adfsResource;

	private IUserManager _userManager;
	private IAuthenticationProviderManager _authenticationProvider;

	/*
cdp.adfs.location=https://accesso.cdptest.it/adfs/oauth2/authorize
cdp.adfs.clientId=fe2bccdd-0c5f-4856-b6d0-59aef738d35d
cdp.adfs.redirectUrl=http://cdprh96:8230/portalexample/
cdp.adfs.resource=http://cdprh96:8230/portalexample/
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		_logger.debug("{} ready", this.getClass().getName());
	}

	@Override
	public int service(RequestContext reqCtx, int status) {
		_logger.debug("Invoked {}", this.getClass().getName());
		int retStatus = ControllerManager.INVALID_STATUS;
		if (status == ControllerManager.ERROR) {
			return status;
		}
		try {
			HttpServletRequest req = reqCtx.getRequest();
			String username = req.getParameter(SystemConstants.LOGIN_USERNAME_PARAM_NAME);
			String password = req.getParameter(SystemConstants.LOGIN_PASSWORD_PARAM_NAME);
			HttpSession session = req.getSession();
			//Punto 1
			if (username != null && password != null) {
				String returnUrl = req.getParameter("returnUrl");
				returnUrl = (null != returnUrl && returnUrl.trim().length() > 0) ? returnUrl : null;
				_logger.debug("user {} - password ******** ", username);
				UserDetails user = this.getAuthenticationProvider().getUser(username, password);
				if (user != null) {
					if (!user.isAccountNotExpired()) {
						req.setAttribute("accountExpired", new Boolean(true));
					} else {
						session.setAttribute(SystemConstants.SESSIONPARAM_CURRENT_USER, user);
						_logger.debug("New user: {}", user.getUsername());
						if (null != returnUrl) {
							return super.redirectUrl(URLDecoder.decode(returnUrl, "ISO-8859-1"), reqCtx);
						}
					}
				} else {
					req.setAttribute("wrongAccountCredential", new Boolean(true));
					if (null != returnUrl) {
						req.setAttribute("returnUrl", returnUrl);
					}
				}
			}
			//Punto 2
			if (session.getAttribute(SystemConstants.SESSIONPARAM_CURRENT_USER) == null) {
				String redirect = this.executeOAuthCall(req);
				if (null != redirect) {
					return super.redirectUrl(URLDecoder.decode(redirect, "ISO-8859-1"), reqCtx);
				}
				UserDetails guestUser = this.getUserManager().getGuestUser();
				session.setAttribute(SystemConstants.SESSIONPARAM_CURRENT_USER, guestUser);
			}
			retStatus = ControllerManager.CONTINUE;
		} catch (Throwable t) {
			_logger.error("Error, could not fulfill the request", t);
			retStatus = ControllerManager.SYS_ERROR;
			reqCtx.setHTTPError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return retStatus;
	}

	protected String executeOAuthCall(HttpServletRequest req) {
		OAuthAuthzResponse oar = null;
		try {
			try {
				oar = OAuthAuthzResponse.oauthCodeAuthzResponse(req);
			} catch (OAuthProblemException e) {
				_logger.error("Errore in recupero codice... o primo ingresso");
				System.out.println("Errore in recupero codice... o primo ingresso");
			}
			String code = (null != oar) ? oar.getCode() : null;
			if (null == code) {
				_logger.info("REDIRECT VERSO ADFS -> ");
				System.out.println("Redirect verso adfs");
				/*
	https://accesso.cdptest.it/adfs/oauth2/authorize?
				response_type=code
				&
				client_id=fe2bccdd-0c5f-4856-b6d0-59aef738d35d
				&
				response_mode=form_post
				&
				redirect_uri=http://cdprh96:8230/
				&
				resource=http://cdprh96:8230/
				 */
 /*
				OAuthClientRequest oauthRequest = OAuthClientRequest
						.authorizationLocation("https://accesso.cdptest.it/adfs/oauth2/authorize")
						.setClientId("fe2bccdd-0c5f-4856-b6d0-59aef738d35d")
						.setRedirectURI("http://cdprh96:8230/")
						.setParameter("resource", "http://cdprh96:8230/")
						.setParameter("response_mode", "form_post")
						.setParameter("response_type", "code")
						.buildQueryMessage();
				 */
				OAuthClientRequest oauthRequest = OAuthClientRequest
						.authorizationLocation(this.getAdfsLocation())
						.setClientId(this.getAdfsClientId())
						.setRedirectURI(this.getAdfsRedirectUrl())
						.setParameter("resource", this.getAdfsResource())
						.setParameter("response_mode", "form_post")
						.setParameter("response_type", "code")
						.buildQueryMessage();

				_logger.info("REDIRECT VERSO ADFS -> " + oauthRequest.getLocationUri());
				System.out.println("Redirect verso adfs -> " + oauthRequest.getLocationUri());
				return oauthRequest.getLocationUri();
			} else {
				_logger.info("CODICE DI RITORNO OAUTH -> " + code);
				System.out.println("CODICE DI RITORNO OAUTH -> " + code);

				OAuthClientRequest oauthRequest = OAuthClientRequest
						.tokenLocation(this.getAdfsLocation())
						//.tokenProvider(OAuthProviderType.MICROSOFT)
						.setGrantType(GrantType.AUTHORIZATION_CODE)
						.setClientId(this.getAdfsClientId())
						//.setClientSecret("your-facebook-application-client-secret")
						.setRedirectURI(this.getAdfsRedirectUrl())
						.setCode(code)
						.buildQueryMessage();
				/*
						.setClientId(this.getAdfsClientId())
						.setRedirectURI(this.getAdfsRedirectUrl())
						.setParameter("resource", this.getAdfsResource())
						.setParameter("response_mode", "form_post")
						.setParameter("response_type", "code")
						.buildQueryMessage();
				 */

				System.out.println("1 richiesta -> " + code);

				OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
				OAuthJSONAccessTokenResponse oAuthResponse = oAuthClient.accessToken(oauthRequest);

				System.out.println("2 richiesta -> " + code);
				System.out.println("----------------------------------------- ");
				String accessToken = oAuthResponse.getAccessToken();
				System.out.println("accessToken -> " + accessToken);
				Long expiresIn = oAuthResponse.getExpiresIn();
				System.out.println("expiresIn -> " + expiresIn);
				System.out.println("----------------------------------------- ");
				return null;
			}
		} catch (Exception e) {
			System.out.println("ERRORE IN INTERROGAZIONE -> " + e.getMessage());
			e.printStackTrace();
			_logger.error("Errore invocazione Endpoint CDP", e);
		}
		return null;
	}

	public String getAdfsLocation() {
		return _adfsLocation;
	}

	public void setAdfsLocation(String adfsLocation) {
		this._adfsLocation = adfsLocation;
	}

	public String getAdfsClientId() {
		return _adfsClientId;
	}

	public void setAdfsClientId(String adfsClientId) {
		this._adfsClientId = adfsClientId;
	}

	public String getAdfsRedirectUrl() {
		return _adfsRedirectUrl;
	}

	public void setAdfsRedirectUrl(String adfsRedirectUrl) {
		this._adfsRedirectUrl = adfsRedirectUrl;
	}

	public String getAdfsResource() {
		return _adfsResource;
	}

	public void setAdfsResource(String adfsResource) {
		this._adfsResource = adfsResource;
	}

	protected IUserManager getUserManager() {
		return _userManager;
	}

	public void setUserManager(IUserManager userManager) {
		this._userManager = userManager;
	}

	protected IAuthenticationProviderManager getAuthenticationProvider() {
		return _authenticationProvider;
	}

	public void setAuthenticationProvider(IAuthenticationProviderManager authenticationProvider) {
		this._authenticationProvider = authenticationProvider;
	}

}
